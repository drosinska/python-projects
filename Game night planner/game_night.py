# create a list of gamers attending a game night

gamers = []

# add gamers to the gamers' list

def add_gamer(gamer, gamers_list):
    if gamer.get("name") and gamer.get("availability"):
        gamers_list.append(gamer)
    else:
        print("The gamer is missing critical information.")

karen = {
    'name': 'Karen Lina',
    'availability': ['Monday', 'Thursday', 'Saturday']
}

metzli = {
    'name': 'Metzli Oluchi',
    'availability': ['Monday', 'Sunday']
}

oliver = {
    'name': 'Oliver Zoja',
    'availability': ["Tuesday", "Thursday", "Sunday"]
}

jaumet = {
    'name': 'Jaumet Praveen',
    'availability': ["Thursday", "Friday", "Saturday"]
}

pili = {
    'name': 'Pili Phyllida',
    'availability': ["Thursday", "Friday", "Saturday"]
}


add_gamer(karen, gamers)
add_gamer(metzli, gamers)
add_gamer(oliver, gamers)
add_gamer(jaumet, gamers)
add_gamer(pili, gamers)


# find best availability

def build_daily_frequency_table():
    return {
        'Monday': 0,
        'Tuesday': 0,
        'Wednesday': 0,
        'Thursday': 0,
        'Friday': 0,
        'Saturday': 0,
        'Sunday': 0
    }


count_availability = build_daily_frequency_table()


def calculate_availability(gamers_list, available_frequency):
    for gamer in gamers_list:
        for day in gamer['availability']:
            available_frequency[day] += 1


calculate_availability(gamers, count_availability)


def find_best_night(availability_table):
    best_availability = 0
    for day, availability in availability_table.items():
        if availability > best_availability:
            best_night = day
            best_availability = availability
    return best_night


game_night = find_best_night(count_availability)



def available_on_night(gamers_list, day):
    return [gamer for gamer in gamers_list if day in gamer['availability']]


attending_game_night = available_on_night(gamers, game_night)


# generate an e-mail for the participants

form_email = """
Dear {name},

We're happy to host "{game}" night and wishes you will attend. Come by on {day_of_week} and have fun!

Yours,
the Gamers Club
"""


def send_email(gamers_who_can_attend, day, game):
    for gamer in gamers_who_can_attend:
        print(form_email.format(name=gamer['name'], day_of_week=day, game=game))


send_email(attending_game_night, game_night, "Icy Tower")

unable_to_attend_best_night = []

second_night_availability = build_daily_frequency_table()

# organize a second game night

unable_to_attend_best_night = [gamer for gamer in gamers if game_night not in gamer['availability']]
second_night_availability = build_daily_frequency_table()
calculate_availability(unable_to_attend_best_night, second_night_availability)
second_night = find_best_night(second_night_availability)

available_second_game_night = available_on_night(gamers, second_night)
send_email(available_second_game_night, second_night, "Icy Tower")