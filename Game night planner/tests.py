import game_night
import unittest


class GameNightTests(unittest.TestCase):

    def test_add_gamer_valid(self):
        gamers = []
        caroline = {
            'name': 'Caro',
            'availability': ['Monday', 'Saturday']
        }
        game_night.add_gamer(caroline, gamers)
        self.assertIn(caroline, gamers, 'caroline should be in gamers')

    def test_add_gamer_invalid(self):
        gamers = []
        caroline = {
            'name': 'Caro',
            'availability': []
        }
        game_night.add_gamer(caroline, gamers)
        self.assertNotIn(caroline, gamers, 'caroline should not be in gamers')

    def test_build_daily_frequency_table(self):
        count_availability = game_night.build_daily_frequency_table()
        self.assertEquals(count_availability, {
            'Monday': 0,
            'Tuesday': 0,
            'Wednesday': 0,
            'Thursday': 0,
            'Friday': 0,
            'Saturday': 0,
            'Sunday': 0
        }, 'count_availability should be {\'Monday\': 0,\'Tuesday\': 0, \'Wednesday\': 0, \'Thursday\': 0, \'Friday\': 0, \'Saturday\': 0, \'Sunday\': 0}')

    def test_calculate_availability(self):
        gamers = [{'name': 'John Praveen', 'availability': ["Tuesday", "Friday", "Sunday"]},
                  {'name': 'Kate Phyllida', 'availability': ["Thursday", "Friday", "Saturday"]}]
        count_availability = {
            'Monday': 0,
            'Tuesday': 0,
            'Wednesday': 0,
            'Thursday': 0,
            'Friday':0,
            'Saturday': 0,
            'Sunday': 0
        }
        game_night.calculate_availability(gamers, count_availability)
        self.assertEquals(count_availability, {
            'Monday': 0,
            'Tuesday': 1,
            'Wednesday': 0,
            'Thursday': 1,
            'Friday': 2,
            'Saturday': 1,
            'Sunday': 1
        }, 'count_availability should be {\'Monday\': 0,\'Tuesday\': 1, \'Wednesday\': 0, \'Thursday\': 1, \'Friday\': 2, \'Saturday\': 1, \'Sunday\': 1}')

    def test_find_best_night(self):
        availability_table = {
            'Monday': 0,
            'Tuesday': 1,
            'Wednesday': 0,
            'Thursday': 1,
            'Friday': 2,
            'Saturday': 1,
            'Sunday': 1
        }
        self.assertEquals(game_night.find_best_night(availability_table), 'Friday',
                          'game_night.find_best_night(availability_table) should return \'Friday\'')

    def test_available_on_night(self):
        gamers = [{'name': 'John Praveen', 'availability': ["Tuesday", "Friday", "Sunday"]},
                  {'name': 'Kate Phyllida', 'availability': ["Thursday", "Friday", "Saturday"]}]
        self.assertEquals(game_night.available_on_night(gamers, 'Saturday'),
                          [{'name': 'Kate Phyllida', 'availability': ["Thursday", "Friday", "Saturday"]}],
                          'game_night.available_on_night(gamers, \'Saturday\') should return [{\'name\': \'Kate Phyllida\', \'availability\': ["Thursday", "Friday", "Saturday"]}]')


unittest.main()
