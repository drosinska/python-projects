class School:
    def __init__(self, name, level, number_of_students):
        self.name = name
        self.level = level
        self.number_of_students = number_of_students

    def get_name(self):
        return self.name

    def get_level(self):
        return self.level

    def get_number_of_students(self):
        return self.number_of_students

    def set_number_of_students(self, new_number_of_students):
        if isinstance(new_number_of_students, int):
            self.number_of_students = new_number_of_students
        else:
            raise TypeError

    def __repr__(self):
        return "A {level} school named {name} with {number_of_students} students.".format(name=self.name, level=self.level, number_of_students=self.number_of_students)


class PrimarySchool(School):
    def __init__(self, name, number_of_students, pickup_policy):
        super().__init__(name, "primary", number_of_students)
        self.pickup_policy = pickup_policy

    def get_pickup_policy(self):
        return self.pickup_policy

    def __repr__(self):
        parentRepr = super().__repr__()
        return parentRepr + "The pickup policy is {pickup_policy}".format(pickup_policy=self.pickup_policy)


class HighSchool(School):
    def __init__(self, name, number_of_students, sports_teams):
        super().__init__(name, "high", number_of_students)
        self.sports_teams = sports_teams

    def get_sports_teams(self):
        return self.sports_teams

    def __repr__(self):
        parentRepr = super().__repr__()
        return parentRepr + "The sports teams include {sports_teams}".format(sports_teams=self.sports_teams)