import unittest
import school_catalogue


class SchoolTests(unittest.TestCase):

    def test_name(self):
        school = school_catalogue.School('Capeside', 'primary', 100)
        self.assertEqual(school.name, 'Capeside', 'school.name should be \'Capeside\'')

    def test_level(self):
        school = school_catalogue.School('Three Hill', 'high', 1000)
        self.assertEqual(school.level, 'high', 'school.level should be \'high\'')

    def test_number_of_students(self):
        school = school_catalogue.School("Capeside", "high", 100)
        self.assertEqual(school.number_of_students, 100, 'school.number_of_students should be 100')


class PrimarySchoolTests(unittest.TestCase):
    
    def test_pickup_policy(self):
        school = school_catalogue.PrimarySchool("Capeside", 100, 'pickup allowed')
        self.assertEqual(school.pickup_policy, 'pickup allowed', 'school.pickup_policy should be \'pickup allowed\'')


class HighSchoolTests(unittest.TestCase):

    def test_sports_teams(self):
        school = school_catalogue.HighSchool('Three Hill', 1000, ['tennis', 'pickleball'])
        self.assertEqual(school.sports_teams, ['tennis', 'pickleball'],
                         'school.sports_teams should be [\'tennis\', \'pickleball\']')


unittest.main()
