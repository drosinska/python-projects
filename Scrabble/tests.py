import scrabble
import unittest


class ScrabbleTests(unittest.TestCase):

    def test_score_words(self):
        word_to_points = {
            'banoffee': 19,
            'pecan': 12,
            'cheesecake': 21
        }
        for word, expected_score in word_to_points.items():
            with self.subTest(word=word):
                score = scrabble.score_word(word)
                self.assertEqual(score, expected_score, 'score should be {}'.format(expected_score))

    def test_update_point_totals(self):
        expected_player_to_points = {'bookworm': 22, 'wordGeek': 28, 'Kid Who Reads': 47, 'Novel Lover': 25}
        self.assertEqual(scrabble.player_to_points, expected_player_to_points,
                         'expected_player_to_points should be {\'bookworm\': 22, \'wordGeek\': 28, \'Kid Who Reads\': 47, \'Novel Lover\': 25}')

    def test_play_word(self):
        scrabble.play_word('Kid Who Reads', 'DOG')
        self.assertEqual(scrabble.player_to_words['Kid Who Reads'][-1], 'DOG',
                         '\'Kid Who Reads\'][-1] should be \'DOG\'')


unittest.main()
