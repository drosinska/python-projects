# builds a point dictionary
letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
           "W", "X", "Y", "Z"]
points = [1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 4, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10]


letters += [letter.lower() for letter in letters]

points *= 2

letter_to_points = {
    key: value
    for key, value
    in zip(letters, points)
}

letter_to_points[" "] = 0
print(letter_to_points)


# scores a word
def score_word(word):
    point_total = 0
    for letter in word:
        point_total += letter_to_points.get(letter, 0)
    return point_total


# scores a game
player_to_words = {
    "bookworm": ["SAIL", "RIGHT", "LAUGH"],
    "wordGeek": ["AIR", "KIND", "DISPLAY"],
    "Kid Who Reads": ["HOUSE", "JAZZ", "NEEDLE"],
    "Novel Lover": ["MILK", "JAR", "STEEL"]
}

player_to_points = {}


def update_point_totals():
    for player, words in player_to_words.items():
        player_points = 0
        for word in words:
            player_points += score_word(word)
        player_to_points[player] = player_points


update_point_totals()
print(player_to_points)


# adds new words to the game
def play_word(player, word):
    player_to_words[player].append(word)


play_word("Kid Who Reads", "CODE")
print(player_to_words)