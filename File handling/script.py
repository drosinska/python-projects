import csv
import json

# create a list of users whose passwords have been compromised
compromised_users = []

# add usernames to the compromised_users list
with open('passwords.csv') as password_file:
    password_csv = csv.DictReader(password_file)
    for password_row in password_csv:
        compromised_users.append(password_row['Username'])

# write usernames to compromised_users.txt
with open('compromised_users.txt', 'w') as compromised_user_file:
    for compromised_user in compromised_users:
        compromised_user_file.write(compromised_user)

# write message to boss_message.json
with open('boss_message.json', 'w') as boss_message:
    boss_message_dict = {
        "recipient": "The Boss",
        "message": "Mission success!"
    }
    json.dump(boss_message_dict, boss_message)

# write message to new_passwords.csv
with open('new_passwords.csv', 'w') as new_passwords_obj:
    hacker_sig = """
 _  _     ___   __  ____             
/ )( \   / __) /  \(_  _)            
) \/ (  ( (_ \(  O ) )(              
\____/   \___/ \__/ (__)             
_  _   __    ___  __ _  ____  ____  
/ )( \ / _\  / __)(  / )(  __)(    \
) __ (/    \( (__  )  (  ) _)  ) D (
\_)(_/\_/\_/ \___)(__\_)(____)(____/
"""
    new_passwords_obj.write(hacker_sig)