# Import module
import sqlite3

# Create connection object
con = sqlite3.connect("hotel_booking.db")

# Create cursor object
cur = con.cursor()

# View first row of booking_summary
print(cur.execute('''SELECT * FROM booking_summary''').fetchone())

# View first ten rows of booking_summary
print(cur.execute('''SELECT * FROM booking_summary''').fetchmany(10))

# Create object bra and print first 5 rows to view data
bra = cur.execute('''SELECT * FROM booking_summary WHERE country = 'BRA';''').fetchmany(5)

# Create new table called bra_customers
cur.execute(
    '''CREATE TABLE IF NOT EXISTS bra_customers(num INTEGER, hotel TEXT, is_cancelled INTEGER, lead_time INTEGER, arrival_date_year INTEGER, arrival_date_month TEXT, arrival_date_day_of_month INTEGER, adults INTEGER, children INTEGER, country TEXT, adr REAL, special_requests INTEGER)''')

# Insert the object bra into the table bra_customers
cur.executemany('''INSERT INTO bra_customers VALUES(?,?,?,?,?,?,?,?,?,?,?,?)''', bra)

# View the first 10 rows of bra_customers
cur.execute('''SELECT * FROM bra_customers''').fetchmany(10)

# Retrieve lead_time rows where the bookings were canceled
lead_time_can = cur.execute('''SELECT lead_time FROM bra_customers WHERE is_cancelled = 1''').fetchall()

# Find average lead time for those who canceled and print results
sum = 0
for lead_time in lead_time_can:
    sum = sum + lead_time[0]
    lead_time_can_avg = sum / len(lead_time_can)

print(lead_time_can_avg)

# Retrieve lead_time rows where the bookings were not canceled
lead_time_not_can = cur.execute('''SELECT lead_time FROM bra_customers WHERE is_cancelled = 0''').fetchall()

# Find average lead time for those who did not cancel and print results
for lead_time in lead_time_not_can:
    sum = sum + lead_time[0]
    lead_time_not_can_avg = sum / len(lead_time_not_can)

print(lead_time_not_can_avg)

# Retrieve special_requests rows where the bookings were canceled
special_requests_can = cur.execute('''SELECT special_requests FROM bra_customers WHERE is_cancelled = 1''').fetchall()

# Find total speacial requests for those who canceled and print results
for special_request in special_requests_can:
    sum = sum + special_request[0]
    requests_can_sum = sum
print(requests_can_sum)

# Retrieve special_requests rows where the bookings were not canceled
special_requests_not_can = cur.execute(
    '''SELECT special_requests FROM bra_customers WHERE is_cancelled = 1''').fetchall()

# Find total special requests for those who did not cancel and print results
for special_request in special_requests_not_can:
    sum = sum + special_request[0]
    requests_not_can_sum = sum
print(requests_not_can_sum)

# Commit changes and close the connection
con.commit()
con.close()