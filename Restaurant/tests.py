import restaurant
import unittest


class BusinessTests(unittest.TestCase):

    def test_name(self):
        franchise = restaurant.Franchise('18 Avenue', restaurant.menus)
        new_business = restaurant.Business('Palermo', franchise)
        self.assertEqual(new_business.name, 'Palermo', 'new_business.name should be \'Palermo\'')


class FranchiseTests(unittest.TestCase):

    def test_address(self):
        new_restaurant = restaurant.Franchise('12 West Road', restaurant.menus)
        self.assertEqual(new_restaurant.address, '12 West Road', 'new_restaurant.address should be \'12 West Road\'')

    def test_available_menus_1600(self):
        """Return menus available at 4 p.m."""
        self.assertEqual(restaurant.flagship_store.available_menus(1600),
                         [restaurant.brunch_menu, restaurant.early_bird_menu, restaurant.kids_menu],
                         "restaurant.flagship_store.available_menus(1600) should return [Dinner menu available from 1700 - 2300]")

    def test_available_menus_2300(self):
        """Return menus available at 11 p.m."""
        self.assertEqual(restaurant.flagship_store.available_menus(2300), [restaurant.dinner_menu],
                         "restaurant.flagship_store.available_menus(2300) should return [Brunch menu available from 1100 - 1600, Early Bird menu available from 1500 - 1800, Kids menu available from 1100 - 2100]")

    def test_available_menus_1000(self):
        """Return menus available at 10 a.m."""
        self.assertEqual(restaurant.flagship_store.available_menus(1000), [],
                         "restaurant.flagship_store.available_menus(1000) should return []")


class MenuTests(unittest.TestCase):

    def test_name(self):
        seasonal_menu = restaurant.Menu('Seasonal',
                                        {'roasted summer vegetables': 11.50, 'strawberry gateau': 10.50,
                                         'asparagus salad': 13.00},
                                        1300, 1800)
        self.assertEqual(seasonal_menu.name, 'Seasonal', 'seasonal_menu.name should be \'Seasonal\'')

    def test_items(self):
        seasonal_menu = restaurant.Menu('Seasonal',
                                        {'roasted summer vegetables': 11.50, 'strawberry gateau': 10.50,
                                         'asparagus salad': 13.00},
                                        1300, 1800)
        self.assertEqual(seasonal_menu.items,
                         {'roasted summer vegetables': 11.50, 'strawberry gateau': 10.50, 'asparagus salad': 13.00},
                         'seasonal_menu.items should be {\'roasted summer vegetables\': 11.50, \'strawberry gateau\': 10.50, \'asparagus salad\': 13.00}')

    def test_start_time(self):
        seasonal_menu = restaurant.Menu('Seasonal',
                                        {'roasted summer vegetables': 11.50, 'strawberry gateau': 10.50,
                                         'asparagus salad': 13.00},
                                        1300, 1800)
        self.assertEqual(seasonal_menu.start_time, 1300, 'seasonal_menu.start_time should be 1300')

    def test_end_time(self):
        seasonal_menu = restaurant.Menu('Seasonal',
                                        {'roasted summer vegetables': 11.50, 'strawberry gateau': 10.50,
                                         'roasted asparagus': 13.00},
                                        1300, 1800)
        self.assertEqual(seasonal_menu.end_time, 1800, 'seasonal_menu.end_time should be 1800')

    def test_calculate_bill(self):
        bill = restaurant.brunch_menu.calculate_bill(['pancakes', 'tea'])
        self.assertEqual(bill, 8.5,
                         "restaurant.brunch_menu.calculate_bill(['pancakes', 'tea']) should return 8.5")

    def test_calculate_bill_empty(self):
        bill = restaurant.dinner_menu.calculate_bill([])
        self.assertEqual(bill, 0,
                         "restaurant.dinner_menu.calculate_bill([]) should return 0")

    def test_calculate_bill_invalid_item(self):
        """Calculates bill containing an invalid item. The invalid item shouldn't be added to the bill."""
        bill = restaurant.early_bird_menu.calculate_bill(['coffee', 'espresso', 'tea'])
        self.assertEqual(bill, 4.5,
                         "restaurant.early_bird_menu.calculate_bill(['coffee', 'espresso', 'tea']) should return 4.5")


unittest.main()
